#!/bin/bash

export XMLLINT_INDENT="    "
xmllint --format ./src/icat_esrf_definitions/hdf5_cfg.xml > hdf5_cfg.xml.new
mv hdf5_cfg.xml.new ./src/icat_esrf_definitions/hdf5_cfg.xml
