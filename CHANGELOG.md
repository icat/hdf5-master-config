# CHANGELOG.md

## 3.0.0 (unreleased)

## v2.1.4

Bug fixes:

- Fix xml formatting

New keys:

- `InstrumentLaser02_repetition_rate`

Modified descriptions:

- `InstrumentLaser01_repetition_rate`

## v2.1.3

New keys:

- `TOMOAcquisition_scan_type`
- `TOMOAcquisition_optic_name`
- `TOMOAcquisition_beam_magnification`
- `TOMOAcquisition_optic_magnified_pixel_size`

Renamed keys:

- `TOMO_technique=TOMOAcquisition_technique`
- `TOMO_experimentType`=`TOMO_experiment_type`
- `TOMO_ZMot`=`TOMOAcquisitionZseries_z_mot`
- `TOMO_ZStart`=`TOMOAcquisitionZseries_z_start`
- `TOMO_ZDelta`=`TOMOAcquisitionZseries_z_delta`
- `TOMO_ZNsteps`=`TOMOAcquisitionZseries_z_n_steps`
- `TOMO_projN`=`TOMOAcquisition_proj_n`
- `TOMO_refN`=`TOMOAcquisition_flat_n`
- `TOMO_darkN`=`TOMOAcquisition_dark_n`
- `TOMO_refOn`=`TOMOAcquisition_flat_on`
- `TOMO_yStep`=`TOMOAcquisition_y_step`
- `TOMO_zStep`=`TOMOAcquisition_z_step`
- `TOMO_FTOMO_PAR`=`TOMO_ftomo_par`
- `TOMO_exposureTime`=`TOMOAcquisition_exposure_time`
- `TOMO_detectorDistance`=`TOMOAcquisition_sample_detector_distance`
- `TOMO_xshutterTime`=`TOMO_xshutter_time`
- `TOMO_energy`=`TOMOAcquisition_energy`
- `TOMO_halfAcquisition`=`TOMOAcquisition_half_acquisition`
- `TOMO_imagesPerStep`=`TOMO_images_per_step`
- `TOMO_interlaced`=`TOMO_interlaced`
- `TOMO_nested`=`TOMO_nested`
- `TOMO_noImagesAtEnd`=`TOMOAcquisition_no_images_at_end`
- `TOMO_readSrcur`=`TOMOAcquisition_read_srcur`
- `TOMO_saveSeparateDarkImage`=`TOMO_save_separate_dark_image`
- `TOMO_sourceSampleDistance`=`TOMOAcquisition_source_sample_distance`
- `TOMO_srcurStart`=`TOMOAcquisition_srcur_start`
- `TOMO_srcurStop`=`TOMOAcquisition_srcur_stop`
- `TOMO_scanRange`=`TOMOAcquisition_scan_range`
- `TOMO_accExposureTime`=`TOMOAcquisition_acc_exposure_time`
- `TOMO_accFramesCount`=`TOMOAcquisition_acc_frames_count`
- `TOMO_accelDisp`=`TOMOAcquisition_accel_disp`
- `TOMO_autoUpdateRef`=`TOMO_auto_update_ref`
- `TOMO_beamCheck`=`TOMOAcquisition_beam_check`
- `TOMO_camXMot`=`TOMOAcquisition_camera_x_mot`
- `TOMO_ccdFlipHorz`=`TOMOAcquisition_camera_flip_horz`
- `TOMO_ccdFlipVert`=`TOMOAcquisition_camera_flip_vert`
- `TOMO_opticsType`=`TOMOAcquisition_optic_type`
- `TOMO_scintillator`=`TOMOAcquisition_scintillator`
- `TOMO_ccdPixelSize`=`TOMOAcquisition_camera_pixel_size`
- `TOMO_latencyTime`=`TOMOAcquisition_latency_time`
- `TOMO_imagesAtEndAsQuali`=`TOMO_images_at_end_as_quali`
- `TOMO_liveCorrection`=`TOMO_live_correction`
- `TOMO_magnification`=`TOMOAcquisition_magnification`
- `TOMO_monoTuneOnRef`=`TOMO_mono_tune_on_ref`
- `TOMO_noAccelCorr`=`TOMO_no_accel_corr`
- `TOMO_openSlitsOnQuali`=`TOMO_open_slits_on_quali`
- `TOMO_opticsEyePiece`=`TOMO_optics_eye_piece`
- `TOMO_refPower`=`TOMO_flat_power`
- `TOMO_readoutTime`=`TOMO_readout_time`
- `TOMO_roundingCorrection`=`TOMO_rounding_correction`
- `TOMO_safeTime`=`TOMO_safe_time`
- `TOMO_shiftTurns`=`TOMO_shift_turns`
- `TOMO_speedCorrFactor`=`TOMO_speed_corr_factor`
- `TOMO_monoTuneOnStart`=`TOMO_mono_tune_on_start`
- `TOMO_software`=`TOMO_soft_version`
- `TOMO_vacuumValue`=`TOMO_vacuum_value`
- `TOMO_vacuumName`=`TOMO_vacuum_name`
- `TOMO_sx0`=`TOMO_sx0`
- `TOMO_ccdTime`=`TOMO_camera_time`
- `TOMO_comment`=`TOMOAcquisition_comment`
- `TOMO_start_angle`=`TOMOAcquisition_start_angle`
- `TOMO_scanGeometry`=`TOMOAcquisition_type`
- `TOMO_x_volume`=`TOMOReconstruction_nb_voxel_x`
- `TOMO_y_volume`=`TOMOReconstruction_nb_voxel_y`
- `TOMO_z_volume`=`TOMOReconstruction_nb_voxel_z`
- `TOMO_scanTime`=`TOMOAcquisition_duration`
- `TOMO_seriesTime`=`TOMOAcquisitionZseries_duration`
- `TOMO_ccdAcqMode`=`TOMOAcquisition_camera_acq_mode`
- `TOMO_pixelSize`=`TOMOAcquisition_sample_pixel_size`
- `TOMO_noRefAtEnd`=`TOMOAcquisition_no_flat_at_end`



## v2.1.2

New keys:

- `InstrumentBeam_incident_beam_divergence`
- `InstrumentBeam_horizontal_incident_beam_divergence`
- `InstrumentBeam_vertical_incident_beam_divergence`
- `InstrumentBeam_final_polarization`
- `InstrumentDetector01_name`
- `InstrumentDetector01_type`
- `InstrumentDetector01_manufacturer`
- `InstrumentDetector01_model`
- `InstrumentDetector02_name`
- `InstrumentDetector02_type`
- `InstrumentDetector02_manufacturer`
- `InstrumentDetector02_model`
- `InstrumentDetector03_name`
- `InstrumentDetector03_type`
- `InstrumentDetector03_manufacturer`
- `InstrumentDetector03_model`
- `InstrumentDetector04_name`
- `InstrumentDetector04_type`
- `InstrumentDetector04_manufacturer`
- `InstrumentDetector04_model`
- `InstrumentDetector05_name`
- `InstrumentDetector05_type`
- `InstrumentDetector05_manufacturer`
- `InstrumentDetector05_model`
- `InstrumentDetector06_name`
- `InstrumentDetector06_type`
- `InstrumentDetector06_manufacturer`
- `InstrumentDetector06_model`
- `InstrumentDetector07_name`
- `InstrumentDetector07_type`
- `InstrumentDetector07_manufacturer`
- `InstrumentDetector07_model`
- `InstrumentDetector08_name`
- `InstrumentDetector08_type`
- `InstrumentDetector08_manufacturer`
- `InstrumentDetector08_model`
- `InstrumentDetector09_name`
- `InstrumentDetector09_type`
- `InstrumentDetector09_manufacturer`
- `InstrumentDetector09_model`
- `InstrumentDetector10_name`
- `InstrumentDetector10_type`
- `InstrumentDetector10_manufacturer`
- `InstrumentDetector10_model`
- `TOMOReconstruction_angle_offset`
- `TOMOReconstruction_angles_file`
- `TOMOReconstruction_axis_correction_file`
- `TOMOReconstruction_centered_axis`
- `TOMOReconstruction_clip_outer_circle`
- `TOMOReconstruction_cor_options`
- `TOMOReconstruction_enable_halftomo`
- `TOMOReconstruction_end_x`
- `TOMOReconstruction_end_y`
- `TOMOReconstruction_end_z`
- `TOMOReconstruction_fbp_filter_cutoff`
- `TOMOReconstruction_fbp_filter_type`
- `TOMOReconstruction_method`
- `TOMOReconstruction_optim_algorithm`
- `TOMOReconstruction_padding_type`
- `TOMOReconstruction_preconditioning_filter`
- `TOMOReconstruction_rotation_axis_position`
- `TOMOReconstruction_start_x`
- `TOMOReconstruction_start_y`
- `TOMOReconstruction_start_z`
- `TOMOReconstruction_translation_movements_file`
- `TOMOReconstruction_weight_tv`
- `TOMOReconstructionPhase_ctf_advanced_params`
- `TOMOReconstructionPhase_ctf_geometry`
- `TOMOReconstructionPhase_delta_beta`
- `TOMOReconstructionPhase_method`
- `TOMOReconstructionPhase_padding_type`
- `TOMOReconstructionPhase_unsharp_coeff`
- `TOMOReconstructionPhase_unsharp_method`
- `TOMOReconstructionPhase_unsharp_sigma`
- `TOMOReconstruction_voxel_size_x`
- `TOMOReconstruction_voxel_size_y`
- `TOMOReconstruction_voxel_size_z`
- `SampleTrackingShipment_name`
- `SampleTrackingShipment_id`
- `SSXJet_size`
- `SSXChip_column_number`
- `Sample_support`
- `SSXChip_row_number`
- `SSXChip_model`
- `SSXJet_speed`
- `DOI_users`
- `SSXChip_horizontal_spacing`
- `InstrumentLaser01_repetition_rate`
- `scanNumber`
- `InstrumentDetector01_frame_time`
- `InstrumentLaser01_wavelength`
- `SSXChip_vertical_spacing`
- `InstrumentLaser02_wavelength`
- `endDate`
- `group_by`
- `machine`
- `software`
- `SamplePaleo_collection_number`
- `TOMO_subframe_nb`
- `MX_characterisation_id`
- `MX_crystalPositionName`
- `MX_kappa_settings_id`
- `MX_position_id`
- `MXMR_max_resolution`
- `MXMR_min_resolution`
- `MXMR_space_group`
- `MXMR_step`
- `MXMRLigandFitting_b_factor`
- `MXMRLigandFitting_free_r`
- `MXMRLigandFitting_GIF_file`
- `MXMRLigandFitting_ligand_name`
- `MXMRLigandFitting_ligand_FOFC_CC`
- `MXMRLigandFitting_ligand_XYZ`
- `MXMRLigandFitting_MAP_2FOFC`
- `MXMRLigandFitting_MAP_FOFC`
- `MXMRLigandFitting_MTZ_file`
- `MXMRLigandFitting_occupancy`
- `MXMRLigandFitting_PDB_file`
- `MXMRLigandFitting_PNG_2FOFC`
- `MXMRLigandFitting_PNG_FOFC`
- `MXMRLigandFitting_r_crystal`
- `MXMRRefinement_free_r`
- `MXMRRefinement_MAP_2FOFC`
- `MXMRRefinement_MAP_FOFC`
- `MXMRRefinement_MTZ_file`
- `MXMRRefinement_PDB_file`
- `MXMRRefinement_r_crystal`
- `MXMRPhasing_best_LLG`
- `MXMRPhasing_best_RFZ`
- `MXMRPhasing_best_TTZ`
- `MXMRPhasing_MAP_2FOFC`
- `MXMRPhasing_MAP_FOFC`
- `MXMRPhasing_monomer_form_count`
- `MXMRPhasing_MTZ_file`
- `MXMRPhasing_PDB_file`
- `MXMRPhasing_RFZ_list`
- `MXMRPhasing_search_model`
- `MXMRPhasing_source`
- `MXMRPhasing_space_group`
- `MXMRPhasing_TFZ_list`
- `MXSAD_average_fragment_length`
- `MXSAD_enantiomorph`
- `MXSAD_cc_partial_model`
- `MXSAD_chain_count`
- `MXSAD_max_resolution`
- `MXSAD_min_resolution`
- `MXSAD_MTZ_file`
- `MXSAD_PDB_file`
- `MXSAD_pseudo_free_cc`
- `MXSAD_residues_count`
- `MXSAD_solvent`
- `MXSAD_space_group`
- `MXSAD_step`

## 2.1.1

Bug fixes:

- use importlib to determine `DEFINITIONS_FILE` correctly in all types of installations

## 2.1.0

New keys:

- `ExternalReferences_neuroglancer`
- `InstrumentLaser01_name`
- `InstrumentLaser01_type`
- `InstrumentLaser01_probe`
- `InstrumentLaser01_energy`
- `InstrumentLaser01_pulse_width`
- `InstrumentLaser02_name`
- `InstrumentLaser02_type`
- `InstrumentLaser02_probe`
- `InstrumentLaser02_energy`
- `InstrumentLaser02_pulse_width`

## 2.0.0

Renamed keys:

- `abstract` has been renamed to `doi_abstract`

Modified descriptions:

- `SampleChanger_position`

New keys:

- `technique_pid`: persistent identifier of the experimental technique (PaNET id's for now)
- `SampleLocalisation_name`
- `SampleLocalisation_country`
- `SampleLocalisation_continental_region`
- `SamplePaleo_scientific_domain`
- `SamplePaleo_repository_institution`
- `SamplePaleoGeologicalTime_formation`
- `SamplePaleoGeologicalTime_era`
- `SamplePaleoGeologicalTime_period`
- `SamplePaleoGeologicalTime_epoch`
- `SamplePaleoClassification_species`
- `SamplePaleoClassification_material_type`
- `SamplePaleoClassification_clade1`
- `SamplePaleoClassification_clade2`
- `SamplePaleoClassification_clade3`
- `SamplePaleoClassification_clade4`
- `SamplePaleoClassification_clade5`
- `SamplePaleoClassification_clade6`
- `ExternalReferencesPublication_doi`
- `ExternalReferencesPublication_endnote`
- `ExternalReferencesDatacollector_endnote`

## 1.0.0

First deployment of [ICAT](https://icatproject.org/) keys defined for the ESRF.
