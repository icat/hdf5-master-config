import xml.etree.ElementTree as ET
import re
import sys


# Function to read the .parameters file and return a set of keys
def read_parameters(file_path):
    with open(file_path, "r") as file:
        return set(line.strip() for line in file if line.strip())


# Function to parse the XML and extract cleaned text
def parse_xml(xml_file_path):
    pattern = re.compile(r"[^a-zA-Z0-9 _]+")
    extracted_texts = []

    try:
        tree = ET.parse(xml_file_path)
        root = tree.getroot()

        for elem in root.iter():
            if elem.text and elem.text.strip():
                cleaned_text = pattern.sub("", elem.text.strip())
                extracted_texts.append(cleaned_text)

    except Exception as e:
        print(f"Error parsing the XML file: {e}")
        sys.exit(1)

    return set(extracted_texts)


# Check if the parameters file and XML file paths were provided
if len(sys.argv) < 4:
    print(
        "Usage: python script_name.py <parameters_file_path> <xml_file_path> <changelog_file_path>"
    )
    sys.exit(1)

# Get the parameters file path, XML file path, and changelog file path from command-line arguments
parameters_file_path = sys.argv[1]
xml_file_path = sys.argv[2]
changelog_file_path = sys.argv[3]
tag = sys.argv[4]
# Read keys from the .parameters file
parameters_keys = read_parameters(parameters_file_path)

# Parse the XML and get the extracted texts
extracted_keys = parse_xml(xml_file_path)

# Compare the keys
missing_keys = parameters_keys - extracted_keys
extra_keys = extracted_keys - parameters_keys

# Prepare the output formatted strings
output_lines = []

output_lines.append(f"\n# {tag}")
if missing_keys:
    output_lines.append("\n Removed keys:")
    output_lines.extend(f"- {key}" for key in missing_keys)

if extra_keys:
    output_lines.append("\nNew keys:")
    output_lines.extend(f"- {key}" for key in extra_keys)

if not missing_keys and not extra_keys:
    output_lines.append("All keys match!")

# Read the existing content of the CHANGELOG.md
try:
    with open(changelog_file_path, "r") as changelog_file:
        existing_content = changelog_file.readlines()
except FileNotFoundError:
    existing_content = []

# Prepend the output to the existing content
with open(changelog_file_path, "w") as changelog_file:
    # Write the new output first
    for line in output_lines:
        changelog_file.write(line + "\n")
    # Write the existing content after the new output
    changelog_file.writelines(existing_content)

print(f"Output prepended to {changelog_file_path}")
