# Contribution guide

## Commit

Install the python dev dependencies before committing anything

```bash
pip install .[dev]
```

Whenever you commit, it will run syntax and validation checks
which you can run manually with

```bash
pre-commit run --all-files
```

To run this automatically every time you commit, install the git hook with

```bash
pre-commit install
```

It uses `xmllint` to auto-format XML files. Install it when missing.
On Debian/Ubuntu-derived distributions this is done with

```bash
sudo apt-get install libxml2-utils
```

## Versioning

The python package version `MAJOR.MINOR.PATCH` follows the [semantic versioning](https://semver.org) guidelines.

* `MAJOR`: ICAT keys are removed or renamed
* `MINOR`: ICAT keys are added
* `PATCH`: ICAT key metadata changed (e.g. description or units)

## Releasing

To release the python project you need `twine`

```bash
pip install twine
```

### Build

Build a source distribution

```bash
rm -rf assets
python3 setup.py sdist -d assets
```

Alternatively, download the latest source distribution build by the CI

```bash
curl -s -o assets.zip -L https://gitlab.esrf.fr/icat/hdf5-master-config/-/jobs/artifacts/master/download?job=assets
rm -rf assets
mkdir assets
unzip assets.zip -d .
```

### Deploy

Deploy on [testpypi](https://test.pypi.org) to [check](https://test.pypi.org/project/icat-esrf-definitions/) that everything looks fine

```bash
twine upload -r testpypi assets/*
```

Deploy on [pypi](https://pypi.org) for [production](https://pypi.org/project/icat-esrf-definitions/)

```bash
twine upload -r pypi assets/*
```

### Tag

Add a git tag one released

```bash
git tag v1.2.3 -m "Release version 1.2.3"
git push && git push --tags
```
