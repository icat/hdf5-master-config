import xmltodict
import sys

# Check if the file path was provided as a parameter
if len(sys.argv) < 2:
    print("Usage: python script_name.py <xml_file_path>")
    sys.exit(1)

# Get the XML file path from command-line arguments
xml_file_path = sys.argv[1]

# Read the XML file
try:
    with open(xml_file_path, "r") as file:
        xml_data = file.read()
except Exception as e:
    print(f"Error reading the XML file: {e}")
    sys.exit(1)

# Parse the XML
try:
    data = xmltodict.parse(xml_data)
except Exception as e:
    print(f"Error parsing the XML file: {e}")
    sys.exit(1)

# Print the parsed data
print(data)

# Access specific nodes if needed
group = data["group"]
print(f"Group attributes: {group['@NX_class']}, {group['@groupName']}")
print(f"Title: {group['title']}")
print(f"Scan Number: {group['scanNumber']}")
print(f"Proposal: {group['proposal']}")
