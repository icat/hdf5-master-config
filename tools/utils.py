import xml.etree.ElementTree as ET
import re
import icat
import icat.config

"""
It returns an array with the text nodes of a XML file
"""


def getParameterListFromFilePath(filePath):
    tree = ET.parse(filePath)
    text = ET.tostring(tree.getroot(), encoding="utf8", method="text")
    noBlankLines = filter(lambda x: not re.match(r"^\s*$", x), text)
    return noBlankLines.replace("$", "\n").replace("{", "").replace("}", "").split("\n")


def getParameterListFromICAT(conf):
    client = icat.Client(conf.url, **conf.client_kwargs)
    client.login(conf.auth, conf.credentials)

    try:
        parameterTypeList = client.search("SELECT (p) FROM ParameterType p")
        return sorted(parameterTypeList, key=lambda k: k.name)
    except icat.exception.ICATPrivilegesError as e:
        print("[ERROR] %s" % (e))
