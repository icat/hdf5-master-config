import xml.etree.ElementTree as ET
import re
import sys

# Check if the file path was provided as a parameter
if len(sys.argv) < 2:
    print("Usage: python script_name.py <xml_file_path>")
    sys.exit(1)

# Get the XML file path from command-line arguments
xml_file_path = sys.argv[1]

# Parse the XML file
try:
    tree = ET.parse(xml_file_path)
    root = tree.getroot()
except Exception as e:
    print(f"Error parsing the XML file: {e}")
    sys.exit(1)

# Regular expression pattern to remove special characters, keeping only alphanumeric, spaces, and underscores
pattern = re.compile(r"[^a-zA-Z0-9 _]+")

# Recursively walk through the XML tree and print cleaned text if available
for elem in root.iter():
    if elem.text and elem.text.strip():  # Check if the node has non-empty text
        cleaned_text = pattern.sub("", elem.text.strip())  # Remove special characters
        print(cleaned_text)
