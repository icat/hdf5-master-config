#! /usr/bin/python
#
# Search the ICAT for all entity types and report the number of
# objects found for each type.
#

import icat
import icat.config
import logging

logging.basicConfig(level=logging.INFO)
logging.getLogger("suds.client").setLevel(logging.CRITICAL)


def getDatasets(conf):
    client = icat.Client(conf.url, **conf.client_kwargs)
    client.login(conf.auth, conf.credentials)

    try:
        return client.search(
            "SELECT dataset from Dataset dataset where dataset.investigation.name like '%id310010%' include dataset.investigation  "
        )
    except icat.exception.ICATPrivilegesError as e:
        print("[ERROR] %s" % (e))


def getInvestigation(conf, proposalName):
    client = icat.Client(conf.url, **conf.client_kwargs)
    client.login(conf.auth, conf.credentials)

    try:
        return client.search(
            "SELECT investigation from Investigation investigation where investigation.title like '%"
            + proposalName
            + "%'  and investigation.visitId = 'cm01' "
        )
    except icat.exception.ICATPrivilegesError as e:
        print("[ERROR] %s" % (e))


datasets = getDatasets(icat.config.Config().getconfig())


def guessProposalByLocation(location):
    if location == "/data":
        return "unknown"
    if location.find("inhouse") == -1:
        return str(location.split("/")[3])
    return str(location.split("/")[4])


proposals = set()

for dataset in datasets:
    guessedProposal = guessProposalByLocation(dataset.location)
    print(dataset.name + r"\t" + dataset.location + r"\t" + guessedProposal)
    print(dataset)
    proposals.add(guessedProposal)
    print("---------")
    print(" Summary ")
    print(len(proposals))
    print(proposals)

for proposalName in proposals:
    proposal = getInvestigation(icat.config.Config().getconfig(), proposalName)
    if proposal != []:
        print("Proposal: " + str(proposal[0].name) + " id: " + str(proposal[0].id))
