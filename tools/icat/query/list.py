#! /usr/bin/python
#
# Search the ICAT for all entity types and report the number of
# objects found for each type.
#

import icat
import icat.config
import logging

logging.basicConfig(level=logging.INFO)
logging.getLogger("suds.client").setLevel(logging.CRITICAL)


def getInvestigations(conf):
    client = icat.Client(conf.url, **conf.client_kwargs)
    client.login(conf.auth, conf.credentials)

    try:
        return client.search(
            "SELECT SUM(datasetParameter.stringValue) from DatasetParameter datasetParameter where datasetParameter.dataset.investigation.id=117586989 and datasetParameter.type.id=122288665"
        )
    except icat.exception.ICATPrivilegesError as e:
        print("[ERROR] %s" % (e))


investigations = getInvestigations(icat.config.Config().getconfig())

print(str(investigations))
