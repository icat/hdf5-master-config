#! /usr/bin/python
#
# Search the ICAT for all entity types and report the number of
# objects found for each type.
#

import xml.etree.ElementTree as ET
import re
import icat
import icat.config
import logging


logging.basicConfig(level=logging.INFO)
logging.getLogger("suds.client").setLevel(logging.CRITICAL)

filePath = "../../../hdf5_cfg.xml"
"""
It returns an array with the text nodes of a XML file
"""


def getParameterListFromFilePath(filePath):
    tree = ET.parse(filePath)
    text = ET.tostring(tree.getroot(), encoding="utf8", method="text")
    noBlankLines = filter(lambda x: not re.match(r"^\s*$", x), text)
    return noBlankLines.replace("$", "\n").replace("{", "").replace("}", "").split("\n")


def getParameterListFromICAT(conf):
    client = icat.Client(conf.url, **conf.client_kwargs)
    client.login(conf.auth, conf.credentials)

    try:
        parameterTypeList = client.search("SELECT (p) FROM ParameterType p")
        parameterList = sorted(parameterTypeList, key=lambda k: k.name)
        nameParameterList = []
        for parameter in parameterList:
            nameParameterList.append(parameter.name)
        return nameParameterList
    except icat.exception.ICATPrivilegesError as e:
        print("[ERROR] %s" % (e))


conf = icat.config.Config().getconfig()


ICATParameterTypeList = getParameterListFromICAT(conf)
XMLParameterTypeList = getParameterListFromFilePath(filePath)

missingDB = list(set(XMLParameterTypeList) - set(ICATParameterTypeList))
if "" in missingDB:
    missingDB.remove("")
missingXML = list(set(ICATParameterTypeList) - set(XMLParameterTypeList))
if "" in missingXML:
    missingXML.remove("")

print(missingDB)
if len(missingDB) > 0:
    print("\n\n[ERROR] These parameters exists in %s but not in the DB" % str(filePath))
    for parameter in sorted(missingDB):
        print(parameter)

print(
    "\n\n[INFO] -----------------------------------------------------------------------------------------------------------"
)
print(
    "[INFO] Summary of metadata on %s database and %s file"
    % (str(conf.url), str(filePath))
)
print(
    "[INFO] Total %s parameters defined in the XML file"
    % (str(len(XMLParameterTypeList)))
)
print(
    "[INFO] Total %s parameters defined in the ICAT DB"
    % (str(len(ICATParameterTypeList)))
)
print(
    "[INFO] -----------------------------------------------------------------------------------------------------------\n\n"
)

if len(missingDB) > 0:
    print(
        "[ERROR] Found %s parameters that shoud be defined in the DB"
        % str(len(missingDB))
    )

if len(missingXML) > 0:
    print(
        "[WARNING] Found %s parameters that are not used in the XML\n\n"
        % str(len(missingXML))
    )
