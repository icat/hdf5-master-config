import xml.etree.ElementTree as ET
import sys


# Function to extract node values and attributes
def extract_node_info(node):
    return {
        "tag": node.tag,
        "text": node.text.strip() if node.text else None,
        "attributes": node.attrib,
    }


# Check if the file path was provided as a parameter
if len(sys.argv) < 2:
    print("Usage: python script_name.py <xml_file_path>")
    sys.exit(1)

# Get the XML file path from command-line arguments
xml_file_path = sys.argv[1]

# Parse the XML file
try:
    tree = ET.parse(xml_file_path)
    root = tree.getroot()
except Exception as e:
    print(f"Error parsing the XML file: {e}")
    sys.exit(1)

# List to store node information
node_list = []

# Recursively walk through the XML tree
for elem in root.iter():
    node_list.append(extract_node_info(elem))

# Print node values and attributes
for node in node_list:
    print(f"Tag: {node['tag']}")
    print(f"Text: {node['text']}")
    print(f"Attributes: {node['attributes']}")
    print("-" * 20)
