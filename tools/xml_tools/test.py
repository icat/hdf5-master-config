import os
import pytest
from tools.xml.validate import parsefile


@pytest.mark.parametrize("filename", ["hdf5_cfg.xml"])
def test_hdf5_cfg(filename):
    dirname = os.path.dirname(__file__)
    filename = os.path.join(dirname, "..", "..", filename)
    assert os.path.isfile(filename)
    parsefile(filename)
