import sys
import argparse
from glob import glob
import xml.parsers.expat
import traceback


def parsefile(file):
    parser = xml.parsers.expat.ParserCreate()
    with open(file, "rb") as fh:
        parser.ParseFile(fh)


def main(argv=None):
    if argv is None:
        argv = sys.argv
    parser = argparse.ArgumentParser(description="Validate ICAT XML files")
    parser.add_argument("filename", help="path or patterm")
    args = parser.parse_args(argv[1:])
    for filename in glob(args.filename):
        try:
            print(filename)
            parsefile(filename)
            print("[PASSED] %s is well-formed" % filename)
        except Exception as e:
            traceback.print_exc()
            print("[ERROR] %s is %s" % (filename, e))
            return 1
    return 0


if __name__ == "__main__":
    sys.exit(main())
