import sys
import argparse
from glob import glob
import lxml.etree as etree


def getSubentries(filename):
    techniques = []
    subentries = etree.parse(filename).xpath('//group[@NX_class="NXsubentry"]')
    for subentry in subentries:
        if "groupName" in subentry.attrib:
            techniques.append(subentry.get("groupName"))
    return techniques


def main(argv=None):
    if argv is None:
        argv = sys.argv
    parser = argparse.ArgumentParser(description="Get techniques from ICAT XML files")
    parser.add_argument("filename", help="path or pattern")
    args = parser.parse_args(argv[1:])

    ret = 0
    for filename in glob(args.filename):
        try:
            techniques = getSubentries(filename)
            print(techniques)
            print("[PASSED] %s is well-formed" % filename)
        except Exception as e:
            print("[ERROR] %s is %s" % (filename, e))
            ret = 1

    return ret


if __name__ == "__main__":
    sys.exit(main())
